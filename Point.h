#ifndef POINT_H
#define POINT_H
#include <iostream>


class Point
{
    private:
        float x, y, z;
        //Point* next;
    public:
        Point(float xx=0,float yy=0,float zz=0);
        Point operator+(const Point&) const;
        Point operator-(const Point&) const;
        Point operator+=(const Point&);
        Point operator-=(const Point&);
        Point operator=(const Point&);
        bool operator==(const Point&);
        bool operator!=(const Point&);
        bool operator>(const Point&);
        bool operator<(const Point&);

        friend
        std::ostream& operator<<(std::ostream&, const Point&);

        virtual ~Point();

    //protected:


};

#endif // POINT_H
