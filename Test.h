#ifndef TEST_H
#define TEST_H


class Test
{
    public:
        Test();
        void pointOperators(void);
        void setOfPoints(void);
        virtual ~Test();

    //protected:
    private:
};

#endif // TEST_H
