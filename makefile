TARGET = points.cpp
CC = g++

$(TARGET): main.o Point.o SetOfPoints.o Test.o
	$(CC) main.o Point.o SetOfPoints.o Test.o -o $(TARGET)

main.o: main.cpp Point.h SetOfPoints.h Test.h
	$(CC) main.cpp -c -o main.o

mathFun.o: SetOfPoints.cpp SetOfPoints.h Point.h
	$(CC) SetOfPoints.cpp -c -o SetOfPoints.o

interface.o: Test.cpp Test.h SetOfPoints.h Point.h
	$(CC) Test.cpp -c -o Test.o

utilities.o: Point.cpp Point.h
	$(CC) Point.cpp -c -o Point.o

.PHONY: clean
clean:
	rm -f *.o
	rm -f $(TARGET)
