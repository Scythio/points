#include "Point.h"

Point::Point(float xx,float yy,float zz)
{
    x=xx;   y=yy;   z=zz;
}

Point Point::operator+(const Point& p) const
{
    Point p2;
    p2.x=(this->x + p.x);
    p2.y=(this->y + p.y);
    p2.z=(this->z + p.z);
    return p2;
}

Point Point::operator-(const Point& p) const
{
    Point p2;
    p2.x=(this->x - p.x);
    p2.y=(this->y - p.y);
    p2.z=(this->z - p.z);
    return p2;
}

Point Point::operator+=(const Point& p)
{
    this->x += p.x;
    this->y += p.y;
    this->z += p.z;
    return *this;
}

Point Point::operator-=(const Point& p)
{
    this->x -= p.x;
    this->y -= p.y;
    this->z -= p.z;
    return *this;
}

Point Point::operator=(const Point& p)
{
    this->x = p.x;
    this->y = p.y;
    this->z = p.z;
    return *this;
}

bool Point::operator==(const Point& p)
{
    if( this->x==p.x  &&  this->y==p.y  &&  this->z==p.z )
    {
    return true;
    }
    else
    {
    return false;
    }
}

bool Point::operator!=(const Point& p)
{
    if( this->x!=p.x  ||  this->y!=p.y  ||  this->z!=p.z )
    {
    return true;
    }
    else
    {
    return false;
    }
}

bool Point::operator<(const Point& p)
{
    if(this->x==p.x)
    {
        if(this->y==p.y)
        {
            if(this->z>=p.z)
            {
                return false;
            }
            else if( this->z<p.z )
            {
                return true;
            }
        }
        else if( this->y<p.y )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else if( this->x<p.x )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Point::operator>(const Point& p)
{
    if(this->x==p.x)
    {
        if(this->y==p.y)
        {
            if(this->z<=p.z)
            {
                return false;
            }
            else if( this->z>p.z )
            {
                return true;
            }
        }
        else if( this->y>p.y )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else if( this->x>p.x )
    {
        return true;
    }
    else
    {
        return false;
    }
}

std::ostream& operator<<(std::ostream& os, const Point& p)
{
    os << p.x << ' ' << p.y << ' ' << p.z;
    return os;
}

Point::~Point()
{
    //dtor
}
