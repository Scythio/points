#include <iostream>
#include "Point.h"
#include "Test.h"

using namespace std;

int main()
{
    Test test;
    test.pointOperators();
    cout<<endl;
    test.setOfPoints();
    return 0;
}

/*
cout << "Hello world!" << endl;
    Point p1;
    p1.coord(1,6,3);
    cout << "p1  " << p1 << endl;
    Point p2;
    p2.coord(2,-4.5,0.4);
    cout << "p2  " << p2 << endl;
    Point p3=p1-p2;
    cout << "p3  " << p3 << endl;
    p3+=p2;
    cout << "p3  " << p3 << endl;
*/
