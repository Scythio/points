#ifndef SETOFPOINTS_H
#define SETOFPOINTS_H
#include "Point.h"

struct Element
{
    Point point;
    Element *next;
};


class SetOfPoints
{
    private:
    Element *head, *tail;
    public:
    SetOfPoints();
    void addPoint(Point p);
    void removePoint(Point p);

    friend
    std::ostream& operator<<(std::ostream&, const SetOfPoints&);
    SetOfPoints operator+(const SetOfPoints&) const;
    SetOfPoints operator*(const SetOfPoints&) const;
    SetOfPoints operator+=(const SetOfPoints&);
    SetOfPoints operator*=(const SetOfPoints&);
    //Point operator+=(const Point&);
    //Point operator*=(const Point&);
    ~SetOfPoints();
};

#endif // SETOFPOINTS_H
