#include <iostream>
#include "SetOfPoints.h"
#include "Point.h"


SetOfPoints::SetOfPoints()
{
      head=NULL;
      tail=NULL;
}

void SetOfPoints::addPoint(Point newPoint)
{
    Element *newElement=new Element;
    Element *target=head;
    Element *prev=head;
    newElement->point=newPoint;
    newElement->next=NULL;
    bool ready=0;
    if(head==NULL)
    {
        head=newElement;
        tail=newElement;
        newElement=NULL;
    }
    else
    {
        while(target!=NULL && !ready)
        {
            if( target->point > newElement->point )
            {
                newElement->next=target;
                if(target==head)
                {
                    head=newElement;
                }
                else
                {
                    prev->next=newElement;
                }
                ready=true;
            }
            else if( target->point == newElement->point )
            {
                ready=true;
                delete newElement;
            }
            if(!ready)
            {
            prev=target;
            target=target->next;
            }
        }
        if(!ready){
            tail->next=newElement;
            tail=newElement;
        }
    }
}

void SetOfPoints::removePoint(Point point2remove)
{
    Element *target=head;
    Element *prev=NULL;
    bool ready=0;
    while(target!=NULL && !ready)
    {
        if( target->point == point2remove)
        {
            if(target==head)
            {
                head=target->next;
            }
            else
            {
                prev->next=target->next;
            }
            ready=true;
            delete target;
        }
        else if( target->point > point2remove )
        {
            ready=true;
            std::cout<< "There was no such a point to be removed." <<std::endl;
        }
        if(!ready)
        {
        prev=target;
        target=target->next;
        }
    }
    if(!ready){
        std::cout<< "There was no such a point to be removed." <<std::endl;
    }

}

std::ostream& operator<<(std::ostream& os, const SetOfPoints& s)
{
    Element *target=s.head;
    while(target!=NULL)
    {
        os << target->point <<std::endl;
        target=target->next;
    }
    return os;
}

SetOfPoints SetOfPoints::operator+(const SetOfPoints& s) const
{
    SetOfPoints s2;
    Element *target=this->head; //.head
    while(target!=NULL)
    {
        s2.addPoint(target->point);
        target=target->next;
    }
    target=s.head; //.head
    while(target!=NULL)
    {
        s2.addPoint(target->point);
        target=target->next;
    }
    return  s2;
}

SetOfPoints SetOfPoints::operator*(const SetOfPoints& s) const
{
    SetOfPoints s2;
    Element *target1=this->head; //.head
    Element *target2=s.head; //.head
    while(target1!=NULL)
    {
        target2=s.head;
        while(target2!=NULL)
        {
            if(target1->point==target2->point)
            {
                s2.addPoint(target1->point);
            }
            target2=target2->next;
        }
        target1=target1->next;
    }
    return  s2;
}

SetOfPoints SetOfPoints::operator+=(const SetOfPoints& s)
{
    *this = *this+s;
    return *this;
}

SetOfPoints SetOfPoints::operator*=(const SetOfPoints& s)
{
    *this = *this*s;
    return *this;
}

SetOfPoints::~SetOfPoints()
{
    Element *target=head;
    Element *ex=NULL;
    while(target!=NULL)
    {
        ex=target;
        target=target->next;
        delete ex;
    }
}
